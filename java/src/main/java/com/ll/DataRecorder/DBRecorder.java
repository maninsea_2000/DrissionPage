package com.ll.DataRecorder;

import javax.naming.NoPermissionException;

/**
 * 用于存储数据到sqlite的工具
 * @author 陆
 * @address <a href="https://t.me/blanksig"/>click
 */
public class DBRecorder extends BaseRecorder {
    @Override
    public void addData(Object data) {

    }

    @Override
    protected void _record() throws NoPermissionException {

    }
}
