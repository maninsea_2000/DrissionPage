（请按照以下模板填报issue，省略号的位置需要填入）

0. 我已给本库打了星星

1. 我本来想借助本库实现一个…………功能；

2. 在实现的过程中我遇到了…………问题；

3. 我已经查阅了[使用文档](http://g1879.gitee.io/drissionpagedocs)，与这个功能相关的章节链接是…………（请把链接粘贴到此处）；

4. 我使用的python版本是……，DrissionPage版本是……；

5. 我的代码截图是：
（请在此处放图片）

6. 我的报错截图是：
（请在此处放图片）
